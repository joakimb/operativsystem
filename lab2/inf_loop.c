#include <stdio.h>
#include <signal.h>
#include <unistd.h>

int volatile proceed = 1;

struct sigaction action;

void handle_alrm(int number){
	proceed = 0;
}

int main (){
	action.sa_handler = handle_alrm;
	sigaction(SIGALRM, &action, NULL);
	int itr = 0;
	alarm(10);
	while(proceed){
		itr++;
	}
	printf("Number of iterations: %d\n", itr);
}