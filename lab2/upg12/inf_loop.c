#include <stdio.h>
#include <signal.h>
#include <unistd.h>

int i = 0;

struct sigaction action;

void handle_usr2(int number){
	printf("Received usr2 %d\n",i);
}
void handle_sigint(int number){
	//sigemptyset(&action.sa_mask);
	printf("Consuming Ctrl-c, starting infinite loop\n");
	for(;;){
		//int i = 0;
		printf("Hey there! %d\n",i);
		sleep(1);
	}
}



int main (){

	sigset_t oldmask;

	sigset_t maskadd;
	sigemptyset(&maskadd);
	sigaddset(&maskadd, SIGUSR1);
	sigaddset(&maskadd, SIGINT);

	if(sigprocmask(SIG_BLOCK, &maskadd, NULL)){
		perror("error setting mask");
	}

	printf("Entering loop\n");
	for(int i = 0; i < 5; i++){
		sleep(1);
	}
	printf("Exiting loop\n");
	sigset_t pending;
	if(sigpending(&pending)){
		perror("error fetching pending");
	}

	for(int i = 1; i < 32; i++){
		if(sigismember(&pending, i)) printf("caught signal: %d\n",i);
	}

}