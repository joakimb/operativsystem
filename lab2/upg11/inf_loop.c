#include <stdio.h>
#include <signal.h>
#include <unistd.h>

int i = 0;

struct sigaction action;

void handle_usr2(int number){
	printf("Received usr2 %d\n",i);
}
void handle_sigint(int number){
	//sigemptyset(&action.sa_mask);
	printf("Consuming Ctrl-c, starting infinite loop\n");
	for(;;){
		printf("Hey there! %d\n",i);
		sleep(1);
	}
}



int main (){
	
	action.sa_handler = handle_sigint;
	sigaction(SIGINT, &action, NULL);
	action.sa_handler = handle_usr2;
	sigaction(SIGUSR2, &action, NULL);
	action.sa_handler = SIG_IGN;
	sigaction(SIGUSR1, &action, NULL);
	//sigemptyset(&action.sa_mask);
	//sigaddset(&action.sa_mask, SIGUSR1);
	
	//if (signal(SIGINT, handle_sigint) == SIG_ERR) printf("Could not set signal handler");
	for(;;){
		int i = 0;
		sleep(1);
	}
}