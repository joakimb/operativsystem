#include <stdio.h>
#include <signal.h>
#include <unistd.h>
void handle_sig(int number){
	printf("Consuming Ctrl-c (%d)\n", number);
}
int main (){
	if (signal(SIGINT, handle_sig) == SIG_ERR) printf("Could not set signal handler");
	for(;;){
		int i = 0;
		sleep(1);
	}
}