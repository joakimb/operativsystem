#define _GNU_SOURCE
#include <stdio.h>
#include "mymalloc.h"
#include <dlfcn.h>
#include <stddef.h>

#define N (8*1024*1024 / sizeof(size_t))

typedef struct list_t list_t;
struct list_t {
	size_t size;
	list_t* next;
	char data[];
};

static size_t pool[N] = {N * sizeof pool[0]};
static list_t avail = {.next = (list_t*)pool};

static void* (*real_malloc)(size_t)=NULL;

void get_malloc(){
	if (!real_malloc)
		real_malloc = dlsym(RTLD_NEXT, "malloc");
    if (!real_malloc)
        fprintf(stderr, "Could not load malloc");
}

void* malloc(size_t size){
	get_malloc();
	printf("mymalloc\n");
	return real_malloc(size);
}